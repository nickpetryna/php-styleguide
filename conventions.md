# Conventions

## Naming Conventions

Names are everywhere in software. We name out variables, out functions, our arguments, classes and packages. We name out source files and the directories that contain them. We name and name an name. Because we do so much of it, we'd better do it well. What follows are some simple rules for creating good names. Here are a few important conventions that we use at Welbi:

- Use Intention-Revealing Names

```
	public $d; // elapsed time in days
```

A better name to this would be something like:

```
	public $elapsed_time_in_days;
	public $days_since_creation;
	public $days_since_modification;
```

Similar to variable, function names should be so discriptive such that documentation is not needed.

Functions like:

```
	/**
	 *  Gets a list of flagged cells.
	 *
	 *  @return array<FlaggedCell>
	 *  @author Reest McGee
	 */

	public function getThem() {
		...
		return array();
	}
```

A function could easily replaced as such:

```
	public function getFlaggedCells(): array {
		...

		return array();
	}
```

The name in this case is very descriptive a does not need to be documentated to be understood.

##### Avoid Disinformation

Programmers must avoid leaving false clues that obscure the meaning of code. We should avoid words whos entreched meanings vary from intended meaning. For example, ```hp```, ```aix```, ```sco``` would be poor variable because they are names of Unix platforms or variants. Even if you are coding a hypotenuse and ```hp``` looks like a good abbreviation, it could be disinformative.

Do not refer to a grouping of accounts as an ```accountList``` unless it's actually a ```List```. The word list means soething specific to programmers. If the container holding the accounts is not actually a List, it may lead to flase conclusions! So ```accountGroup``` or ```bunchOfAccounts``` or just plain ```accounts``` would be better.

##### Make Meaningful Distinctions

Programmers create problems for themselves when they write code solely to satisfy a compiler or intepreter. For example, because you can't use the same name to refer to two different things in the same scope, you might be tempted to change one name in a arbitrary way.

Number-series naming (a1, a1, ...aN) is opposite of intentional naming. Such names are not disinformative-they are noninformative; they provide no clue to the author's intention.

Another bad example can also appear in the following way:

```
get_active_account();
get_active_accounts();
get_active_account_info();
```

It is hard to distinguish between each functions and how they are different.

##### Use Pronounceable Names

Variable, function and class names should be rememberable, therefore they should be easily pronounceable.

##### Use Searchable Names

Searchable names help development speed and keep this unilaterally understood.

##### Member Prefixes

Theres are strictly forbidden.

##### Class names

Classes and objects should have noun or noun phrase names like ```Customer```, ```WikiPage```, ```Account```, and ```AddressParser```. Avoid words like ```Manager```, ```Processor```, ```Data```, or ```Info``` in the name of a class. A class name should not be a verb.

##### Method names

Methods should have a verb or verb phrase names like ```post_payment```, ```delete_page```, or ```save```. Accessors, mutators, and predicates should be named for their value and *prefixed with get, set and is*.

When constructors are overloaded, use static factory methods with names that describe arguments. For example,

```
fulcrumPoint = Complex.from_real_number(23.0);
```

is generally better than

```
fulcrumPoint = new Complex(23.0);
```

####### Don't be cute

Cuteness in code often appears in the form of colloquialisms or slang. For example, don't use the name ```whack()``` to mean ```kill()```. Don't tell little culture-dependent jokes like ```eatMyShorts()```to mean ```abort()```.



## Functions

##### Small!

The first rule of functions is that they should be small. The second rule of functions is that **they should be smaller than that**.

##### Do one thing

This implies that the blocks within ```if``` statement, ```else``` statements, ```while``` statements, and so on should be one line long. Probably that line should be a function call. Not only does this keep the enclosing function small, but is also adds documentary value because the function called within the block can have a nicely descriptive name.

This also implies that functions should not be large enough to hold nested structures. Therefore, the indent level of a function should not be greater than one or two. This, of course, makes the functions easier to read and understand.

*Functions should do one thing. They should do it well. They should do it only.*

##### One level of abstraction per function

In order to make sure out functions are doing "one thing", we need to make sure that the statements within our functions are all at the same level of abstraction.

##### Switch Statements

The solution to clean switch statements is to bury the switch statement in the basement o an abstract factory, and never let anyone see it. The factory will use the switch statement to create appropriate instances of the derivatives of any class.

###### Use Descriptive names

Similar rules to the naming conventions section, functions names should be incredibly descriptive. Long function names are preferable to documentation. It is hard to overestimate the value of good names. Don't be afraid to make a long name. A long descriptive name is better than a short enigmatic name. A long descriptive name is better than a long descriptive comment.

##### Function arguments

The best functions have no arguments. Next is 1 argument. Then 2. No functions should ever need 3 arguments.

When a function seems to need more that two arguments, it is likely that some of those arguments ought to be wrapped into a class of their own. Consider, for example, the difference between the two following functions:

```
	makeCircle(float $x, float $y, $float $radius) { ... }
	makeCircle(Point $center, $float $radius) { ... }
```

Reducing the number of arguments by creating objects out of them may seem like cheating, but it's not. When groups of variables are passed together, the way ```x``` and ```y``` are in the example above, they are likely part of a concept that eserves a name of its own.

##### Have No Side Effects

Side effects are lies. Your function promises to do one thing, but it also does other **hidden** thing. Your function should be clear to exactly is it doing, and avoid causing side effects whenever possible.

##### Prefer Execptions to Returning Error Codes

The title of this section is very clear. It is never acceptable to return error codes, instead Exception's should be thrown.

##### Extract try/catch blocks

```try-catch``` blocks are ugly in their own right. They confuse the structure of the code and mix error processing with normal processing. So it is better to extract the bodies of the try and catch out into functions of their own . 

##### Error Handling is one Thing

Functions should do one thing. Error handling is one thing. Thus, a function that handles errors should do nothing else.

##### Don't repeat yourself

This one is obvious. Duplication is the root to all evil in software.

## Comments

##### Comments do not make up for bad code
##### Good comments

Some commens are necessary and benefical. 

##### Bad comments

- Mumbling
- Redundant comments
- Misleading comments
- Journal comments
- Closing brace comments
- HTML comments (these come straight from hell)
- Too much information


##### Rules to live by

**Don't use comments when you can use a function or variable**. This is major point. The name of the function should document itself, therefore a correctly name function should never had to be commented where it is defined or called. 
